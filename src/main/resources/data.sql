--Drone initial data
insert into drone (id, serial_number, drone_model, weight_limit, battery_capacity, drone_state, created) values (1, '1234abc','LIGHT_WEIGHT',125.0 , 100, 'IDLE', current_timestamp);
insert into drone (id, serial_number, drone_model, weight_limit, battery_capacity, drone_state, created) values (2, '5678def','LIGHT_WEIGHT',125.0 , 100, 'LOADING', current_timestamp);
insert into drone (id, serial_number, drone_model, weight_limit, battery_capacity, drone_state, created) values (3, '147mnb','MIDDLE_WEIGHT',250.0 , 100, 'IDLE', current_timestamp);
insert into drone (id, serial_number, drone_model, weight_limit, battery_capacity, drone_state, created) values (4, '258vcx','MIDDLE_WEIGHT',250.0 , 100, 'LOADED', current_timestamp);
insert into drone (id, serial_number, drone_model, weight_limit, battery_capacity, drone_state, created) values (5, '369lkj','CRUISER_WIGHT',375.0 , 100, 'IDLE', current_timestamp);
insert into drone (id, serial_number, drone_model, weight_limit, battery_capacity, drone_state, created) values (6, '789hgf','CRUISER_WIGHT',375.0 , 100, 'DELIVERING', current_timestamp);
insert into drone (id, serial_number, drone_model, weight_limit, battery_capacity, drone_state, created) values (7, '456dsa','HEAVY_WEIGHT',500.0 , 100, 'IDLE', current_timestamp);
insert into drone (id, serial_number, drone_model, weight_limit, battery_capacity, drone_state, created) values (8, '123qaz','HEAVY_WEIGHT',500.0 , 100, 'DELIVERED', current_timestamp);
insert into drone (id, serial_number, drone_model, weight_limit, battery_capacity, drone_state, created) values (9, '159wsx','LIGHT_WEIGHT',125.0 , 100, 'IDLE', current_timestamp);
insert into drone (id, serial_number, drone_model, weight_limit, battery_capacity, drone_state, created) values (10, '753edc','LIGHT_WEIGHT', 125.0 , 100, 'IDLE', current_timestamp);

--Medication initial data
insert into medication (id, name, weight, code, image, created) values (1, 'Medicine-1', 50.0, 'MIDICINE_1', null, current_timestamp);