package com.musala.drones.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Max;

import org.hibernate.annotations.CreationTimestamp;

import com.musala.drones.util.DroneModel;
import com.musala.drones.util.DroneState;

@Entity
@Table(name = "drone")
public class Drone {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "serial_number", nullable = false, unique = true, length = 100)
	private String serialNumber;

	@Column(name = "drone_model", nullable = false)
	@Enumerated(EnumType.STRING)
	private DroneModel droneModel;

	@Column(name = "weight_limit", precision = 5, scale = 2, nullable = false)
	@DecimalMax(value = "500.00")
	private BigDecimal weightLimit;

	@Column(name = "battery_capacity", nullable = false)
	@Max(value = 100)
	private int batteryCapacity;

	@Column(name = "drone_state", nullable = false)
	@Enumerated(EnumType.STRING)
	private DroneState droneState;

	@Column(name = "created")
	@CreationTimestamp
	private Date created;

	@OneToMany(mappedBy = "drone")
	private List<Load> droneLoad;

	@Override
	public String toString() {
		return "Drone [id=" + id + ", serialNumber=" + serialNumber + ", droneModel=" + droneModel + ", weightLimit="
				+ weightLimit + ", batteryCapacity=" + batteryCapacity + ", droneState=" + droneState + ", created="
				+ created + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public DroneModel getDroneModel() {
		return droneModel;
	}

	public void setDroneModel(DroneModel droneModel) {
		this.droneModel = droneModel;
	}

	public BigDecimal getWeightLimit() {
		return weightLimit;
	}

	public void setWeightLimit(BigDecimal weightLimit) {
		this.weightLimit = weightLimit;
	}

	public int getBatteryCapacity() {
		return batteryCapacity;
	}

	public void setBatteryCapacity(int batteryCapacity) {
		this.batteryCapacity = batteryCapacity;
	}

	public DroneState getDroneState() {
		return droneState;
	}

	public void setDroneState(DroneState droneState) {
		this.droneState = droneState;
	}

	public List<Load> getDroneLoad() {
		return droneLoad;
	}

	public void setDroneLoad(List<Load> droneLoad) {
		this.droneLoad = droneLoad;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

}
