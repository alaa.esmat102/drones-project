package com.musala.drones.service;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.DroneBatteryLevelLog;
import com.musala.drones.exception.AlreadyExistsException;
import com.musala.drones.exception.BadRequestException;
import com.musala.drones.exception.NotFoundException;
import com.musala.drones.repo.BatteryLevelLogRepository;
import com.musala.drones.repo.DroneRepository;
import com.musala.drones.util.DroneState;

@Service
public class DroneService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DroneService.class);

	@Autowired
	private DroneRepository droneRepository;

	@Autowired
	private BatteryLevelLogRepository batteryLevelLogRepository;

	@Value("${page.size}")
	private int pageSize;

	public Drone registerDrone(Drone drone) {

		LOGGER.info("Registering drone with serial number = {}", drone.getSerialNumber());

		if (Objects.isNull(drone.getId())) {

			Drone dbDrone = droneRepository.getBySerialNumber(drone.getSerialNumber());

			if (dbDrone == null) {

				return droneRepository.save(drone);
			} else {

				throw new AlreadyExistsException(
						String.format("Drone with serialnumber: %s already exists", drone.getSerialNumber()));
			}
		} else {
			throw new BadRequestException(String.format("Drone with id : %s bad request", drone.getId()));
		}
	}

	public List<Drone> checkDronesForLoading() {

		List<Drone> drones = droneRepository.getBydroneState(DroneState.IDLE);

		LOGGER.info("drones available for loading {}", drones);

		return drones;

	}

	public int checkBatteryByDroneId(Integer id) {

		Drone drone = droneRepository.getById(id);

		if (Objects.nonNull(drone)) {

			LOGGER.info("drone  with id {} battery level {}", id, drone.getBatteryCapacity());

			return drone.getBatteryCapacity();

		} else {
			throw new NotFoundException(String.format("Drone with id : %s  was not found", id));
		}

	}

	@Scheduled(fixedDelayString = "${application.drone-battery-check}")
	public void checkDronesBatteryLevel() {

		Page<Drone> page = null;

		int pageNum = 0;

		do {

			page = droneRepository.findAll(PageRequest.of(pageNum, pageSize));

			page.getContent().stream().forEach(drone -> {

				DroneBatteryLevelLog batteryLog = new DroneBatteryLevelLog();

				batteryLog.setDrone(drone);
				batteryLog.setBatteryLevel(drone.getBatteryCapacity());

				batteryLevelLogRepository.save(batteryLog);
			});

			pageNum++;

		} while (pageNum < page.getTotalPages());
	}

}
