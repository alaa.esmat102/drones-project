package com.musala.drones.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.Load;
import com.musala.drones.domain.Medication;
import com.musala.drones.exception.BadRequestException;
import com.musala.drones.exception.NotFoundException;
import com.musala.drones.repo.DroneRepository;
import com.musala.drones.repo.LoadRepository;
import com.musala.drones.repo.MedicationRepository;
import com.musala.drones.util.DroneState;

@Service
public class LoadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoadService.class);

	@Autowired
	private DroneRepository droneRepository;

	@Autowired
	private MedicationRepository medicationRepository;

	@Autowired
	private LoadRepository loadRepository;

	public Load loadDroneWithMedication(String droneSerialNumber, List<Medication> medications) {

		LOGGER.info("Loading  drone  with serial number = {} with medication list ", droneSerialNumber, medications);

		BigDecimal medicationListWeight = new BigDecimal(0.0);

		Drone drone = droneRepository.getBySerialNumber(droneSerialNumber);

		checkDroneForLoading(drone, droneSerialNumber);

		if (medications.isEmpty()) {

			throw new BadRequestException("Medications list can't be empty");
		}

		List<Medication> medicationsList = new ArrayList<Medication>();

		for (Medication medication : medications) {

			Medication dbmedication = medicationRepository.getByCode(medication.getCode());

			if (Objects.nonNull(dbmedication)) {

				medicationsList.add(dbmedication);

				medicationListWeight = medicationListWeight.add(dbmedication.getWeight());

			} else {
				throw new NotFoundException(String.format("Medication with code : %s not found", medication.getCode()));
			}
		}

		if (medicationListWeight.compareTo(drone.getWeightLimit()) <= 0) {

			drone.setDroneState(DroneState.LOADING);

			Load load = new Load();

			load.setDrone(drone);

			load.setMedications(medicationsList);

			return loadRepository.save(load);

		} else {
			throw new BadRequestException(
					String.format("Medications total weight: %s exceeds drone max weight limit: %s",
							medicationListWeight, drone.getWeightLimit()));
		}
	}

	private void checkDroneForLoading(Drone drone, String serialNumber) {

		if (Objects.isNull(drone)) {

			throw new NotFoundException(String.format(" Drone with serial number : %s  not found", serialNumber));
		}

		if (drone.getDroneState() != DroneState.IDLE) {

			throw new BadRequestException(String.format("Drone with serial number : %s is not IDLE", serialNumber));
		}
		if (drone.getBatteryCapacity() < 25) {

			throw new BadRequestException(String.format(" Drone with serial number : %s low battery", serialNumber));
		}
	}

	public List<Medication> checkLoadedMedication(String serialNumber) {

		LOGGER.info("Checking loaded medications for drone with serial number {}", serialNumber);

		Drone dbDrone = droneRepository.getBySerialNumber(serialNumber);

		Load load = loadRepository.getLatestLoadByDroneId(dbDrone.getId());

		List<Medication> medications = load.getMedications();

		return medications;
	}

}
