package com.musala.drones.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.musala.drones.domain.Medication;
import com.musala.drones.exception.AlreadyExistsException;
import com.musala.drones.exception.BadRequestException;
import com.musala.drones.repo.MedicationRepository;

@Service
public class MedicationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);

	@Autowired
	private MedicationRepository medicationRepository;

	public Medication addMedication(Medication medication) {

		LOGGER.info("adding  Medication  with code = {}", medication.getCode());

		if (Objects.isNull(medication.getId())) {

			Medication dbMedication = medicationRepository.getByCode(medication.getCode());

			if (Objects.isNull(dbMedication)) {

				return dbMedication = medicationRepository.save(medication);
			} else {

				throw new AlreadyExistsException(
						String.format(" Medication with code : %s already saved before ", medication.getCode()));
			}

		} else {

			throw new BadRequestException(
					String.format("Medication with id: %s has a bad request", medication.getId()));

		}

	}

	public Medication addMedication(String name, String code, BigDecimal weight, MultipartFile image) {

		LOGGER.info("adding  Medication  with code = {}", code);

		Medication dbMedication = medicationRepository.getByCode(code);

		if (Objects.isNull(dbMedication)) {

			dbMedication = new Medication();
			dbMedication.setName(name);
			dbMedication.setCode(code);
			dbMedication.setWeight(weight);

			try {

				dbMedication.setImage(image.getBytes());

			} catch (IOException e) {
				LOGGER.error(e.getMessage(), e);
			}

			return medicationRepository.save(dbMedication);

		} else {

			throw new AlreadyExistsException(
					String.format(" Medication with code : %s already saved before ", code));
		}

	}
}
