package com.musala.drones.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;

public class LoadDTO {

	private Integer id;

	@CreationTimestamp
	private Date created;

    @NotNull(message = "can't be null")
	private DroneDTO drone;

    @NotNull(message = "can't be null")
	private List<MedicationDTO> medications;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<MedicationDTO> getMedications() {
		return medications;
	}

	public void setMedications(List<MedicationDTO> medications) {
		this.medications = medications;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public DroneDTO getDrone() {
		return drone;
	}

	public void setDrone(DroneDTO drone) {
		this.drone = drone;
	}

	@Override
	public String toString() {
		return "LoadDTO [id=" + id + ", created=" + created + ", drone=" + drone + ", medications=" + medications + "]";
	}

}
