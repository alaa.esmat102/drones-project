package com.musala.drones.dto;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import com.musala.drones.util.DroneModel;
import com.musala.drones.util.DroneState;

public class DroneDTO {

	private Integer id;

	@Length(min = 1, max = 100, message = "can't be less than 1 or more than 100 characters")
	@NotBlank(message = "can't be null or empty")
	private String serialNumber;

	@NotNull(message = "can't be null or empty")
	private DroneModel droneModel;

	@NotNull
	@DecimalMax(value = "500.00", message = "max value is 500")
	@Positive(message = "can't be a negative value")
	private BigDecimal weightLimit;

	@NotNull
	@Positive(message = "can't be a negative value")
	@Max(value = 100, message = "max value is 100")
	private int batteryCapacity;

	@NotNull(message = "can't be null or empty")
	private DroneState droneState;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public DroneModel getDroneModel() {
		return droneModel;
	}

	public void setDroneModel(DroneModel droneModel) {
		this.droneModel = droneModel;
	}

	public BigDecimal getWeightLimit() {
		return weightLimit;
	}

	public void setWeightLimit(BigDecimal weightLimit) {
		this.weightLimit = weightLimit;
	}

	public int getBatteryCapacity() {
		return batteryCapacity;
	}

	public void setBatteryCapacity(int batteryCapacity) {
		this.batteryCapacity = batteryCapacity;
	}

	public DroneState getDroneState() {
		return droneState;
	}

	public void setDroneState(DroneState droneState) {
		this.droneState = droneState;
	}

	@Override
	public String toString() {
		return "DroneDTO [id=" + id + ", serialNumber=" + serialNumber + ", droneModel=" + droneModel + ", weightLimit="
				+ weightLimit + ", batteryCapacity=" + batteryCapacity + ", droneState=" + droneState + "]";
	}

}
