package com.musala.drones.dto;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

public class MedicationDTO {

	private Integer id;

	@NotBlank(message = "can't be null or empty")
	@Pattern(regexp = "^[\\w-]+[^\\s\\W][\\w-]+$", message = "only letters, numbers, '_' and  '-' are allowed")
	private String name;

	@Positive(message = "can't be a negative value")
	@NotNull(message = "can't be null")
	private BigDecimal weight;

	@NotBlank(message = "can't be null or empty")
	@Pattern(regexp = "^[A-Z0-9_]+[^\\s\\W][A-Z0-9_]+$", message = "only upper case letters, numbers and '_' are allowed")
	private String code;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "MedicationDTO [id=" + id + ", name=" + name + ", weight=" + weight + ", code=" + code + "]";
	}

}
