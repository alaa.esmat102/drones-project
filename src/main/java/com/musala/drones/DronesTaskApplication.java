package com.musala.drones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@ComponentScan(basePackages = {"com.musala.drones"})
@EntityScan(basePackages = {"com.musala.drones.domain"})
@EnableJpaRepositories(basePackages = {"com.musala.drones.repo"})
@SpringBootApplication
public class DronesTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(DronesTaskApplication.class, args);
	}

}
