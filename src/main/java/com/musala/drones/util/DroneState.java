package com.musala.drones.util;

public enum DroneState {
	
	IDLE,LOADING,LOADED,DELIVERING,DELIVERED,RETURNING

}
