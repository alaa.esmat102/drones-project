package com.musala.drones.converter;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.Medication;
import com.musala.drones.dto.DroneDTO;
import com.musala.drones.dto.MedicationDTO;

@Component
public class DtoToEntityConverter {

	private ModelMapper modelMapper = new ModelMapper();

	public Drone getEntity(DroneDTO droneDTO) {

		if (Objects.isNull(droneDTO)) {
			return null;
		} else {
			return modelMapper.map(droneDTO, Drone.class);
		}
	}

	public Medication getEntity(MedicationDTO medicationDTO) {

		if (Objects.isNull(medicationDTO)) {
			return null;
		} else {
			return modelMapper.map(medicationDTO, Medication.class);
		}
	}

	public List<Medication> getEntityList(List<MedicationDTO> medicationsDTO) {

		if (Objects.isNull(medicationsDTO) || medicationsDTO.isEmpty()) {

			return Collections.emptyList();
		} else {
			return medicationsDTO.stream().filter(Objects::nonNull).map(this::getEntity).collect(Collectors.toList());
		}
	}
}
