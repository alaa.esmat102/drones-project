package com.musala.drones.converter;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.Load;
import com.musala.drones.domain.Medication;
import com.musala.drones.dto.DroneDTO;
import com.musala.drones.dto.LoadDTO;
import com.musala.drones.dto.MedicationDTO;

@Component
public class EntityToDtoConverter {

	private ModelMapper modelMapper = new ModelMapper();

	public DroneDTO getDto(Drone drone) {

		if (Objects.isNull(drone)) {
			return null;
		} else {
			return modelMapper.map(drone, DroneDTO.class);
		}
	}

	public MedicationDTO getDto(Medication medication) {

		if (Objects.isNull(medication)) {
			return null;
		} else {
			return modelMapper.map(medication, MedicationDTO.class);
		}
	}
	
	public LoadDTO getDTO(Load load) {

		if (Objects.isNull(load)) {

			return null;
		} else {

			return modelMapper.map(load, LoadDTO.class);
		}
	}
	
	public List<DroneDTO> getDTOList(List<Drone> drones) {

		if (Objects.isNull(drones) || drones.isEmpty()) {

			return Collections.emptyList();
		} else {
			return drones.stream().filter(Objects::nonNull).map(this::getDto).collect(Collectors.toList());
		}
	}
	public List<MedicationDTO> getMedicationDTOList(List<Medication> medications) {

		if (Objects.isNull(medications) || medications.isEmpty()) {

			return Collections.emptyList();
		} else {
			return medications.stream().filter(Objects::nonNull).map(this::getDto).collect(Collectors.toList());
		}
	}
}
