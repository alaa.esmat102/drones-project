package com.musala.drones.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.musala.drones.domain.Load;


@Repository
public interface LoadRepository extends JpaRepository<Load, Integer> {
	
	
	Load getByDroneId(Integer id);

	@Query(nativeQuery = true, value = "select * from load where drone_id = :droneId and created = (select max(created) from load where drone_id = :droneId)")
	Load getLatestLoadByDroneId(@Param("droneId") Integer droneId);
}
