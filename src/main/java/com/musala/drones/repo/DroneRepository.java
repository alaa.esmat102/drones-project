package com.musala.drones.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.musala.drones.domain.Drone;
import com.musala.drones.util.DroneState;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Integer> {

	Drone getById(Integer id);

	Drone getBySerialNumber(String serialNumber);
	
	List<Drone>getBydroneState(DroneState state);

}
