package com.musala.drones.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.musala.drones.domain.Medication;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Integer> {
	
	Medication getByCode(String code);

}
