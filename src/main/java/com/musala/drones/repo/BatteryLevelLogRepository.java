package com.musala.drones.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.musala.drones.domain.DroneBatteryLevelLog;

@Repository
public interface BatteryLevelLogRepository extends JpaRepository<DroneBatteryLevelLog, Integer> {

}
