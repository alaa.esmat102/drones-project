package com.musala.drones.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.musala.drones.converter.EntityToDtoConverter;
import com.musala.drones.domain.Medication;
import com.musala.drones.dto.MedicationDTO;
import com.musala.drones.service.LoadService;
import com.musala.drones.service.MedicationService;

@RestController
@RequestMapping("/medications")
public class MedicationController {

	@Autowired
	private MedicationService medicationService;
	
	@Autowired
	private LoadService loadService;

	@Autowired
	private EntityToDtoConverter entityToDtoConverter;

	@PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public MedicationDTO addMedication(
			@RequestParam("name") @Pattern(regexp = "^[\\w-]+[^\\s\\W][\\w-]+$") String name,
			@RequestParam("code")  @Pattern(regexp = "^[A-Z0-9_]+[^\\s\\W][A-Z0-9_]+$") String code,
			@RequestParam("weight") @DecimalMax(value = "500.00") BigDecimal weight,
			@RequestPart MultipartFile image) {

		Medication medication = medicationService.addMedication(name, code, weight, image);

		return entityToDtoConverter.getDto(medication);

	}

	@GetMapping(path = "/{serialNumber}",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MedicationDTO> checkLoadedMedication(@PathVariable  String serialNumber) {

		List<Medication> medications = loadService.checkLoadedMedication(serialNumber);

		return entityToDtoConverter.getMedicationDTOList(medications);
	}
}