package com.musala.drones.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.musala.drones.converter.DtoToEntityConverter;
import com.musala.drones.converter.EntityToDtoConverter;
import com.musala.drones.domain.Drone;
import com.musala.drones.domain.Load;
import com.musala.drones.dto.DroneDTO;
import com.musala.drones.dto.LoadDTO;
import com.musala.drones.dto.MedicationDTO;
import com.musala.drones.service.DroneService;
import com.musala.drones.service.LoadService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/drones")
public class DroneController {

	@Autowired
	private DroneService droneService;
	
	@Autowired
	private LoadService loadService;

	@Autowired
	private DtoToEntityConverter dtoToEntityConverter;

	@Autowired
	private EntityToDtoConverter entityToDtoConverter;


	@ApiOperation(value = "Creates a new drone")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public DroneDTO registerDrone(@RequestBody @Valid DroneDTO droneDto) {

		Drone dbDrone = droneService.registerDrone(dtoToEntityConverter.getEntity(droneDto));

		return entityToDtoConverter.getDto(dbDrone);
	}
	
	@PostMapping(path = "/{serialNumber}/load", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public LoadDTO loadDrone(@PathVariable("serialNumber") String SerialNumber, @RequestBody @Valid List<MedicationDTO> medications) {

		Load load = loadService.loadDroneWithMedication(SerialNumber, dtoToEntityConverter.getEntityList(medications));

		return entityToDtoConverter.getDTO(load);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<DroneDTO> checkDronesForLoading () {
		
		return entityToDtoConverter.getDTOList(droneService.checkDronesForLoading());
	}
	
	@GetMapping(path = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public int checkBatteryByDroneId ( @PathVariable Integer id) {
		
		return droneService.checkBatteryByDroneId(id);
		
		
		
	}

}
