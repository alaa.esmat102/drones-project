package com.musala.drones;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.musala.drones.domain.Medication;
import com.musala.drones.exception.AlreadyExistsException;
import com.musala.drones.exception.BadRequestException;
import com.musala.drones.repo.MedicationRepository;
import com.musala.drones.service.MedicationService;

@SpringBootTest
class MedicationTest {

	@Mock
	MedicationRepository medicationRepository;

	@InjectMocks
	MedicationService medicationService;

	Medication medication;

	@BeforeEach
	void init() {
		medication = new Medication();
		medication.setCode("MIDICINE_1");
		medication.setName("medicine");
		medication.setWeight(new BigDecimal(50.0));
	}

	@Test
	void testAddMedication() {

		when(medicationRepository.save(medication)).thenReturn(medication);

		Medication actualMedication = medicationService.addMedication(medication);

		assertEquals("MIDICINE_1", actualMedication.getCode());
		assertEquals("medicine", actualMedication.getName());
		assertEquals(new BigDecimal(50.0), actualMedication.getWeight());
	}

	@Test
	void testAddAlreadyExistingMedication() {

		when(medicationRepository.getByCode("MIDICINE_1")).thenReturn(medication);

		assertThrows(AlreadyExistsException.class, () -> medicationService.addMedication(medication));
	}

}
