package com.musala.drones;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.musala.drones.domain.Drone;
import com.musala.drones.exception.AlreadyExistsException;
import com.musala.drones.exception.BadRequestException;
import com.musala.drones.exception.NotFoundException;
import com.musala.drones.repo.DroneRepository;
import com.musala.drones.service.DroneService;
import com.musala.drones.util.DroneModel;
import com.musala.drones.util.DroneState;

@SpringBootTest
class DronesTest {

	@Mock
	DroneRepository droneRepo;

	@InjectMocks
	DroneService droneService;

	Drone drone;

	@BeforeEach
	void init() {

		drone = new Drone();
		drone.setBatteryCapacity(100);
		drone.setSerialNumber("123qwe");
		drone.setDroneModel(DroneModel.LIGHT_WEIGHT);
		drone.setDroneState(DroneState.IDLE);
		drone.setWeightLimit(new BigDecimal(125.0));
	}

	@Test
	void testRegisterDroneWithId() {

		drone.setId(1);

		when(droneRepo.save(drone)).thenReturn(drone);

		assertThrows(BadRequestException.class, () -> droneService.registerDrone(drone));

	}

	@Test
	void testRegisterAlreadyExistingDrone() {

		when(droneRepo.getBySerialNumber("123qwe")).thenReturn(drone);

		assertThrows(AlreadyExistsException.class, () -> droneService.registerDrone(drone));

	}

	@Test
	void testCheckNonExistingDroneBatteryLevel() {

		when(droneRepo.getById(Mockito.anyInt())).thenReturn(null);

		assertThrows(NotFoundException.class, () -> droneService.checkBatteryByDroneId(1));
	}

}
