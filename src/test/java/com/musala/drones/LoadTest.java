package com.musala.drones;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.Load;
import com.musala.drones.domain.Medication;
import com.musala.drones.exception.BadRequestException;
import com.musala.drones.exception.NotFoundException;
import com.musala.drones.repo.DroneRepository;
import com.musala.drones.repo.LoadRepository;
import com.musala.drones.repo.MedicationRepository;
import com.musala.drones.service.LoadService;
import com.musala.drones.util.DroneModel;
import com.musala.drones.util.DroneState;

@SpringBootTest
class LoadTest {

	@Mock
	LoadRepository loadRepository;
	@Mock
	DroneRepository droneRepository;
	@Mock
	MedicationRepository  medicationRepository;

	@InjectMocks
	LoadService loadService;

	Drone drone;
	Medication medication1;
	Medication medication2;
	Load load;

	List<Medication> medications ;

	@BeforeEach
	void init() {

		drone = new Drone();
		drone.setBatteryCapacity(100);
		drone.setSerialNumber("123qwe");
		drone.setDroneModel(DroneModel.LIGHT_WEIGHT);
		drone.setDroneState(DroneState.IDLE);
		drone.setWeightLimit(new BigDecimal(125.0));

		medication1 = new Medication();
		medication1.setCode("MIDICINE_1");
		medication1.setName("medicine");
		medication1.setWeight(new BigDecimal(50.0));

		medication2 = new Medication();
		medication2.setCode("MIDICINE_2");
		medication2.setName("medicine");
		medication2.setWeight(new BigDecimal(100.0));

		medications = new ArrayList<Medication>();
		medications.add(medication1);
		medications.add(medication2);
	}

	@Test
	void testLoadNonExistingDroneWithMedication() {

		when(droneRepository.getBySerialNumber(Mockito.anyString())).thenReturn(null);

		assertThrows(NotFoundException.class, () -> loadService.loadDroneWithMedication(drone.getSerialNumber(), medications));
	}


	@Test
	void testLoadNonIdleDroneWithMedication() {

		drone.setDroneState(DroneState.LOADING);

		when(droneRepository.getBySerialNumber(Mockito.anyString())).thenReturn(drone);

		assertThrows(BadRequestException.class, () -> loadService.loadDroneWithMedication(drone.getSerialNumber(), medications));
	}

	@Test
	void testLoadLowBatteryDroneWithMedication() {

		drone.setBatteryCapacity(15);

		when(droneRepository.getBySerialNumber(Mockito.anyString())).thenReturn(drone);

		assertThrows(BadRequestException.class, () -> loadService.loadDroneWithMedication(drone.getSerialNumber(), medications));
	}

	@Test
	void testLoadDroneWithEmptyMedicationList() {

		when(droneRepository.getBySerialNumber(Mockito.anyString())).thenReturn(drone);

		assertThrows(BadRequestException.class, () -> loadService.loadDroneWithMedication(drone.getSerialNumber(), Collections.emptyList()));
	}

	@Test
	void testOverLoadDroneWithMedication() {

		when(droneRepository.getBySerialNumber(Mockito.anyString())).thenReturn(drone);
		when(medicationRepository.getByCode(medication1.getCode())).thenReturn(medication1);
		when(medicationRepository.getByCode(medication2.getCode())).thenReturn(medication2);

		load = new Load();
		load.setDrone(drone);
		load.setMedications(medications);

		assertThrows(BadRequestException.class, () -> loadService.loadDroneWithMedication(drone.getSerialNumber(), medications));

	}

}
